
from flask import Flask,request
import json
from bot import Bot
from flask_socketio import SocketIO

PAGE_ACCESS_TOKEN = 'EAAIkWFBbJtgBAM38Bf68qutdRptZBxcHerK4utZBDscszdzJOoBDyaaHQGtfqDi8kZArGcSU4Gy5eIGq8ZB1szYZBu1YwmlwwSWWmsYb8Ic9CQZB33hBEG5RX4VWHbTV0V9t7XIK0yZCrZAL1dsZBZCbwRc2C1ZBqu7EAwpt5D7lGbW5v4xAiMHJhlT'

GREETINGS = ['hi','hello','hey','chao']

app = Flask(__name__)

app.secret_key = 'secret'
socketio = SocketIO(app,cors_allowed_origins="*")

@app.route('/',methods=['GET','POST'])
def webhook():

    if request.method == 'GET':
        print('---->GET')
        token = request.args.get('hub.verify_token')
        challenge = request.args.get('hub.challenge')
        print(token)

        if token == 'secret':
            return str(challenge)
        return '400'
    else:
        print('---->GET')
        data = json.loads(request.data)
        json_formatted_str = json.dumps(data,indent=2)
        print(json_formatted_str)
        print(json_formatted_str)
        messaging_events = data['entry'][0]['messaging']
        print('-->{}'.format(messaging_events))

        # print(messaging_events)
        # print(messaging_events)
        # if messaging_events:
        #     print('message')
        # else:
        #     print("feed")

        bot = Bot(PAGE_ACCESS_TOKEN)
        for message in messaging_events:

            user_id = message['sender']['id']
            text_input = message['message'].get('text')
            response_text = "I am still learning"
            # handle_send_from_user('user')
            if text_input in GREETINGS:
                response_text = 'Hello, Welcome to my first bot!'
            
            bot.send_text_message(user_id,response_text)

        return '200'
@socketio.on('sendms')
def handle_send_message_event(data):
    socketio.emit('receive_message',data,room=data['room'])

if __name__ == '__main__':
    app.run(debug=True)

    

