
import requests
import json

FACEBOOK_GRAPH_SEND_API = 'https://graph.facebook.com/v8.0/me/'

class Bot(object):

    def __init__(self,access_token,api_url=FACEBOOK_GRAPH_SEND_API):
        self.access_token = access_token
        self.api_url = api_url

    def send_text_message(self,psid,message,messaging_type="RESPONSE"):
        headers = {
            'Content-Type':'application/json'
        }

        data = {
            'messaging_type':messaging_type,
            'recipient':{'id':psid},
            'message':{'text':message}
        }

        params = {'access_token':self.access_token}
        self.api_url = self.api_url + 'messages'
        response = requests.post(self.api_url,
                                headers=headers,
                                params=params,
                                data=json.dumps(data)
                                )

        print(response.content)



# bot = Bot('EAAIkWFBbJtgBAM38Bf68qutdRptZBxcHerK4utZBDscszdzJOoBDyaaHQGtfqDi8kZArGcSU4Gy5eIGq8ZB1szYZBu1YwmlwwSWWmsYb8Ic9CQZB33hBEG5RX4VWHbTV0V9t7XIK0yZCrZAL1dsZBZCbwRc2C1ZBqu7EAwpt5D7lGbW5v4xAiMHJhlT')
# bot.send_text_message(3351266518269863,'sending...')

